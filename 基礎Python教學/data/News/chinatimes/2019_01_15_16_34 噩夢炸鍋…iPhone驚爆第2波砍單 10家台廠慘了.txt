噩夢炸鍋…iPhone驚爆第2波砍單 10家台廠慘了
2019年01月15日 16:34
繼上周日經新聞報導，蘋果3款新iPhone本季將減產10%後，又爆出砍單消息，外資摩根大通（小摩）發布最新報告指出，iPhone將掀起第2波砍單，10檔台廠蘋概股將面臨下調目標價的命運。

其實小摩原本就不看好新款iPhone的銷售，加上屢傳買氣慘淡的壞消息，因此操刀這波台灣蘋果供應鏈報告的小摩科技產業研究部主管Gokul Hariharan認為，iPhone面臨第2波砍單的機率高，還出重手狂降10家台廠評價。

台股權值王台積電首當其衝，小摩將其目標價從265元降到245元，但投資評級仍「優於大盤」，然而另兩家外資砍價更無情，摩根士丹利下調目標價，從214元一口氣下修至199元，外資券商Bernstein下調台積電目標價至225元。

其他重量級蘋概股，也難逃小摩砍目標價，鴻海85元降到76元，日月光67元降到60元，聯電11元降至9.5元，和碩55元降至40元，前2家投資評級「中立」、後2家「劣於大盤」。

可成260元降至190元，緯創18元降至17元，穩懋180元降至155元，瑞儀50元降至48元，美律125元降至120元。

目前摩根大通預估iPhone首季出貨將掉到4100萬支，第2季僅剩3500萬支，今、明兩年iPhone EMS（電子代工）廠出貨都降到1.85億支。

小摩分析，智慧手機的需求疲弱將不會僅限於蘋果，半導體產業將遭遇逆風，Hariharan認為，半導體面臨庫存調整問題，今年營收可能較去年衰退，台積電先進製程放量不如預期，預期台積電17日法說釋出營運展望，今年以美元計價營收成長僅1%，遜於近幾年5%甚至更高的水準。
(中時電子報)