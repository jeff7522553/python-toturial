捷運局：C1／D1土地開發案甄選過程均合法 欣光華產權爭議不影響招商開發
2019年01月15日 16:58
台北市捷運局今（15）日傍晚發出聲明指出，C1／D1土地開發案「台北雙子星」的甄選過程，一切都合法，欣光華產權爭議不影響招商開發。

今（15）日有媒體報導指出，中央投資股份有限公司指稱「欣光華對於這次招標文件的諸多疑義，捷運局均未回應處理，即率然啟動並完成投資人甄選事宜，悖於雙方契約關係及委任關係。106年增補契約書，以及涉及最重要土地分配權益轉換的107年3月1日修正原則，欣光華並未簽字等。」

對此，台北市捷運局傍晚鄭重澄清，C1／D1開發案係依大眾捷運法及其相關規定公告徵求投資人合作開發，該局與欣光華公司之間於辦理本開發案間，並無欣光華公司所稱與捷運局間有委任關係，從招商規劃開始即召開多次地主說明會並建立雙向溝通管道，針對招商作業期間欣光華公司來函所提相關意見，該局均予以回覆澄清。

另外，無論欣光華公司是否簽訂增補契約書，未來仍須依與臺北市政府已簽訂的土地聯合開發契約辦理本土地開發案。C1／D1土地開發案甄選過程均合法，北市捷運局也將依預定進度辦理。

北市捷運局表示，目前中投公司就黨產會所為認定附隨組織及中投公司應收歸國有之處分尚在行政訴訟程序中，如判決結果欣光華公司為土地之合法所有人，欣光華公司應依土地聯合開發契約參與本案開發，否則應負賠償責任外，市府亦得依約取得其所有C1街廓內土地；惟如判決欣光華公司所擁有之土地係為政府所有土地，市府可依法撥用取得土地所有權。

因此，捷運局說明，未來無論訴訟判決結果是否認定為黨產，均不影響本案後續開發與執行。C1／D1開發案完成後屬原可分配欣光華公司之產權歸屬，則將視中投公司與黨產會訴訟判決結果而定。

捷運局表示，至於權益分配部分，依投資契約約定在不影響本府權益下，自得由投資人與地主自行協商，亦無欣光華公司指稱權益受損之情況。對於中投公司來函所述與事實不符，捷運局將嚴正予以駁斥，未來將在既有合約基礎下執行。

北市捷運局表示，C1／D1土地開發案招商過程一切公正公平、公開透明，並在廉政平台的監督下，確保所有甄選過程均合法，本次成功招標後更加深外資法人對於投資臺灣的信心和興趣，該局也將依預定進度與最優申請人簽約，並達成政府、地主、投資人、社會大眾均贏之目標。
(工商 )