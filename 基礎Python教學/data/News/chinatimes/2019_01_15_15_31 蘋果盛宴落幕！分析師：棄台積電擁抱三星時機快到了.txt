蘋果盛宴落幕！分析師：棄台積電擁抱三星時機快到了
2019年01月15日 15:31
台積電今年上半年營運恐怕持續遭遇逆風，多家外資齊聲唱衰，摩根士丹利(大摩)分析師Charlie Chan將其目標價從214元降至199元。銀河聯昌證券分析師詹逸群(Peter Chan)也警告，蘋果等智慧型手機需求疲弱，台積電未來情況恐變得更糟，放棄台積電轉投資三星等其他科技股的時間點可能快到了。
根據美國財經媒體報導，詹逸群在去年10月獨排眾議，將台積電評級降至減持，目標價下調至209元，自他降評迄今，台積電股價已下跌約7.8%。詹逸群強調，他對台積電的看法沒有太大改變，從去年第3季到現在，情況沒有好轉，反而可能變得更糟，由於台積電估值仍高，可能會有越來越多投資人放棄台積電，轉為投資三星或其他成長性更高的美國科技股。
「對台積電來說，也許未來會有可以取代手機的業務，但我目前還沒看到。」詹逸群分析指出，手機相關應用預估約占台積電營收的6成，今年手機整體出貨應該會呈現下滑，台積電2019年預測恐令人失望。
而從新興市場科技股來看，詹逸群認為，南韓高科技股的估值比台積電便宜，比如三星的股價淨值比僅1倍，且記憶體持續有新的應用跟需求，可能將成為市場投資新寵。
(中時電子報)
