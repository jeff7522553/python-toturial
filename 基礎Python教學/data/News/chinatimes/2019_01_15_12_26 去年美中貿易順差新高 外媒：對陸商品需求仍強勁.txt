去年美中貿易順差新高 外媒：對陸商品需求仍強勁
2019年01月15日 12:26
參考消息網1月15日報導外媒指出，中國政府1月14日公布的貿易資料顯示，2018年中國對美國貿易順差創新高。資料顯示出美國對中國產品的旺盛需求，也反映出外國商品面對中國商品的價格和品質沒有競爭優勢。

據美國消費者新聞與商業頻道1月13日報導，中國政府資料顯示，2018年，中國對美國貿易順差年成長17.2％，達到3,233.2億美元。路透社指出，這是2006年以來的最高紀錄。

2018年中國對美國出口額上升11.3％，從美國的進口額成長0.7％。

另據華爾街日報網站1月15日報導，1月14日中國政府公布的貿易資料顯示，川普政府為縮小兩國貿易逆差而發起的貿易攻勢不敵美國對中國產品的旺盛需求。

報導指出，雖然中國的資料顯示2018年中國對美國的貿易順差創出歷史新高，但從中反映出的中美貿易不平衡情況，通常不如美國資料顯示得那麼嚴重。由於計算方法不同，兩國貿易資料並不完全相符。透過香港和其他中轉地的非直接貿易，是資料不一致的原因之一。兩國資料都不包含服務貿易，中國商務部表示，如果計入服務貿易，中美貿易情況將更加平衡。
(工商 )