恒生指數6連漲 報26,667.27點
2019年01月11日 16:29
美股昨晚大漲支持港股表現，恒指11日收盤漲145.84點，報26,667.27點，為連續第6個交易日上揚，國企指數收盤亦漲60.36點，報10,454.95點。在個股方面，受人民幣升值影響，航空股造好，國航大漲8.2％、南航漲6.1％。
(工商 )