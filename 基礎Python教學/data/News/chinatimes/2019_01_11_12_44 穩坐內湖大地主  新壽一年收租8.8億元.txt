穩坐內湖大地主  新壽一年收租8.8億元
2019年01月11日 12:44
內湖大地主。新光人壽重兵布局內湖，共投資14棟大樓，如明基電通、輝達等都是新壽內湖的房客，14棟大樓一年可為新壽帶來8.8億元的租金，新壽目前也與華固合建內湖陽光街的高級住宅，預計2021年完工，屆時還會實現一波不動產收益。

新光人壽2011年標下基泰之星REIT，取得原麗湖飯店的大樓，去年初租約到期，新壽重新整理後成為新光人壽新金湖大樓，首波將新光銀行南京松安分行及新壽內湖通訊處遷入，11日舉行剪彩儀式，新光金控副董事長李紀珠、新光銀行董事長李增昌及總經理謝長融、新光人壽蔡雄繼等主持剪彩儀式。

新光金強調，將新光銀行分行與新壽內湖通訊處合署辦公，未來雙引擎整合行銷綜效，擴大客戶服務，且一樓的開放空間將會定期舉辦講座。

新壽在內湖已有14棟大樓，另外2019年預期新北市板橋區的油庫口三棟大樓、三重水漾大樓都將完工，2020士林見潭大樓及重慶南路高住宅也將完工，不動產開發及活化會持續進行。
(工商 )