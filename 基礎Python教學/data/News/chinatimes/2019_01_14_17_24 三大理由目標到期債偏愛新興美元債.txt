三大理由目標到期債偏愛新興美元債
2019年01月14日 17:24
年關將近，境內基金募集卻越來越熱，過去境內基金發行募集，經常避開農曆年前，以因應資金調度需求，但今年卻有許多基金趕在1月份募集發行，且普遍都為目標到期債券，持續搶市。

資產管理業者分析，隨著美國為首的全球股債市劇烈震盪，加上景氣循環進入中後段，在謹慎保守心態趨使下，這類鎖利型債券產品可能持續延燒。

瀚亞3至6年目標到期新興市場債券基金經理人杜振宇表示，目標到期債券基金普遍以新興美元債作為標的，主要因素有三，包括較高殖利率收益及債券信用評等大多以投資等級債及搭配部分高收益債配置、新興美元債可避開當地貨幣匯率風險、債券違約率相對低。

杜振宇指出，就違約率來看，新興高收益債券違約率僅1.1％，低於美元高收益債，甚至低於亞高收債與全球高收益債券。以新興債市歷史低點進場回測，持有3年以上報酬率可期，統計自2010年以來，新興美元債共3次債息殖利率突破7％，若逢低布局並持有超過3年，則累積回報率可期。

杜振宇認為，新興美元債已經歷大幅修正，目前殖利率來到金融海嘯以來新高，若能債券持有到期、不違約的情況下，收益率目標更為明確，可避開升息和短線市場波動，自然也是投資人趨之若鶩的關鍵。
(工商 )