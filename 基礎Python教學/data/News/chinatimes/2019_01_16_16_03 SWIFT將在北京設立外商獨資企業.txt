SWIFT將在北京設立外商獨資企業
2019年01月16日 16:03
北京市政府與環球同業銀行金融電訊協會（SWIFT）16日在北京簽署合作備忘錄。根據雙方的合作備忘錄，SWIFT將在京設立外商獨資企業，從比利時總部承接中國區業務，為中國用戶提供本地化的服務。該公司成立后，將加入中國支付清算協會並由中國人民銀行依法進行監督管理。

中國人民銀行副行長范一飛指出，在中國成立全資子公司並落戶北京有利於SWIFT在中國業務邁上新台階，標誌著北京市全面推進服務業擴大開放、提升開放型經濟營商環境的新進展，也是人民銀行統籌管理金融基礎設施的重要一步。
(工商 )