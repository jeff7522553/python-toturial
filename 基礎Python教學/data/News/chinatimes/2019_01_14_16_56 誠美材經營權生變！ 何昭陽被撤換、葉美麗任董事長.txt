誠美材經營權生變！ 何昭陽被撤換、葉美麗任董事長
2019年01月14日 16:56
誠美材14日召開董事會，董事提出臨時動議，改選葉美麗擔任董事長，梁基磐擔任副董事長，現任董事長何昭陽遭到撤換。何昭陽反擊表示，該會議已經延期，會主張此次董事會無效。而且董事當中僅3人同意，未達門檻，也無法律效力！

其中獨立董事蔡蒔銓也對提案持保留意見，他指出，因臨時提出，何董事長又缺席本次董事會，無法聽取雙方說明，也無法詳細了解相關情節及對公司的影響，本人難以評估本項臨時動議對公司正面及負面的效果。本人認為本項臨時提出之時機並不適當。既然沒有足夠的資訊及時間評估本項臨時動議對公司的影響，本人對本項臨時動議無法表示意見，無法參與表決。
(工商 )