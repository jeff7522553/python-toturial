華映停工衝擊  12月營收僅9.4億元寫下單月低點
2019年01月10日 18:54
華映10日公佈2018年12月份自行結算合併營收為新台幣9.4億元，較上月份合併營收減少30.7％，較去年同期合併營收減少65.4％。華映2018年12月份大尺寸面板合併出貨量約為1.4萬片，較上月份減少82％，中小尺寸面板合併出貨量約為456萬片，較上月份減少51.4％。

華映去年12月13日宣布聲請重整以及緊急處分，但是因為供應商斷料，因此在12月16日停工。12月19日供應商恢復供貨，華映著手復工，不過投片量不大，因此12月營收銳減。
(工商 )