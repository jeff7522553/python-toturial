元大證獲頒證交所上市證券承銷商第一名
2019年01月16日 19:06
證交所16日舉辦「107年上市家族聯誼會暨中介機構頒獎典禮」表揚績優中介機構。元大證券繼上個月18日勇奪櫃買中心「推薦輔導上櫃及興櫃績效獎」第一名大獎後，本次再奪證交所「流通證券獎-上市證券承銷商第一名」，為107年推動上市櫃雙料冠軍。

元大證券投資銀行業務部致力於輔導並推薦優質企業進入資本市場，於107年推動上市的計有榮科(4989)、緯穎(6669)、中揚光(6668)及騰輝電子-KY(6672)等4家公司，其中，榮科為專業電解銅箔廠，專精於生產高良率利基型銅箔。緯穎主要從事研發、銷售大型資料中心之整機櫃伺服器，主要客戶群為雲端服務商，近幾年營收、獲利呈現倍數成長，為近期IPO募資規模相對較大的案件。

中揚光為光學鏡片模具大廠，著重在手機及車用鏡頭等應用產品，是交易市場上久違的光學類股新兵。

騰輝電子-KY為利基型銅箔基板廠商，選擇不與同業在紅海市場競爭的營運模式，開發出國際汽車大廠認可的散熱鋁基板、符合太空總署及航空公司認可之銅箔基板及黏合片等藍海市場。

元大證券獲奬不斷，繼近期獲《亞元雜誌》頒發「臺灣最佳證券商」及「臺灣最佳企業及投資銀行」等獎項，又分別拿下櫃買中心及獲證交所等兩大資本市場主管機關頒發證券商類的第一名，顯示國內券商已具備走上國際金融舞臺的專業實力。

元大證指出，公司不斷追求創新，並積極協助主管機關健全資本市場發展。未來將持續輔導並推薦優質企業進入資本市場，持續展現元大證券努力為客戶服務、促進資本市場發展及提升台灣金融市場國際競爭力的決心。
(工商 )